var geoJSON;

var getTweet = function(id) {
  $.ajax({
          url: "https://api.twitter.com/1.1/statuses/oembed.json?"
          + "omit_script=false&align=center&hide_media=0&id=" + id,
          dataType: "jsonp",
          success: function(data){
            var twId = data.url.split('/')[data.url.split('/').length - 1]
            $('#tweets').prepend('<div class="' + twId + ' tweetpost">' + data.html 
              + '<span class="tomap">Na mapě</span></div>')
            $('.tweetpost').on('click', function(e){
              var twId = e.currentTarget.className.split(' ')[0]
              for (var i = 0; i < geoJSON.features.length; i++) {
                  if (geoJSON.features[i].properties.id_str == twId) {
                    var coords = geoJSON.features[i].geometry.coordinates;
                    map.setView([coords[1], coords[0]], 12);
                  };
              };
            })
          }
    });
};

var cols = {'JanSmidcro': '#b2182b', 'JanBumba': '#4d4d4d', 'Lenkakab': '#4575b4'}

var urlAll = 'https://datazurnal.cz/arcgis/rest/services/usvolby_tweety/FeatureServer/0/query?where=%28geolocated+%3D+%27true%27%29+AND+%28%28user_screen_name+%3D+%27Lenkakab%27%29+OR+%28user_screen_name+%3D+%27JanSmidcro%27%29+OR+%28user_screen_name+%3D+%27JanBumba%27%29%29&time=&outFields=*&returnGeometry=true&orderByFields=created_at&f=geojson'
var urlLenkakab = 'https://datazurnal.cz/arcgis/rest/services/usvolby_tweety/FeatureServer/0/query?where=%28geolocated+%3D+%27true%27%29+AND+%28%28user_screen_name+%3D+%27Lenkakab%27%29%29&time=&outFields=*&returnGeometry=true&orderByFields=created_at&f=geojson'
var urlJanSmidcro = 'https://datazurnal.cz/arcgis/rest/services/usvolby_tweety/FeatureServer/0/query?where=%28geolocated+%3D+%27true%27%29+AND+%28%28user_screen_name+%3D+%27JanSmidcro%27%29%29&time=&outFields=*&returnGeometry=true&orderByFields=created_at&f=geojson'
var urlJanBumba = 'https://datazurnal.cz/arcgis/rest/services/usvolby_tweety/FeatureServer/0/query?where=%28geolocated+%3D+%27true%27%29+AND+%28%28user_screen_name+%3D+%27JanBumba%27%29%29&time=&outFields=*&returnGeometry=true&orderByFields=created_at&f=geojson'


var drawMap = function(url) {
  $('#map').empty();
  $('#tweets').empty();
  map = L.map("map").setView([39.809734, -98.555620], 4);
  L.esri.basemapLayer("Topographic").addTo(map);
  map.scrollWheelZoom.disable();
  var markers = L.markerClusterGroup();
  $.getJSON(url)
  .done(function (data) {
    geoJSON = data;
    if (data.features.length == 0) {
      $('#tweets').html('Uživatel zatím nemá žádné tweety z USA.')
    } else {
      for (var i = 0; i < data.features.length; i++) {
        var ft = data.features[i];
        var id = ft.properties.id_str;
        getTweet(ft.properties.id_str)
        var author = ft.properties.user_screen_name
        var marker = L.circleMarker(new L.LatLng(ft.geometry.coordinates[1], ft.geometry.coordinates[0]), {
          color: cols[author],
          weight: 1,
          opacity: 1,
          fillOpacity: 0.6,
          radius: 8,
          fillColor: cols[author],
          fillOpacity: 0.4,
          twid: id, 
          auth: author 
        });
        markers.addLayer(marker);
      };

      map.addLayer(markers);

      markers.on('click', function(e) {
        var twId = e.layer.options.twid;
        $('#tweets').scrollTo('.' + twId);
      });
    }
  });
};

drawMap(urlAll);

$('#selector').on('click', function(e){
  map.remove();
  drawMap(window["url" + e.target.className])
});